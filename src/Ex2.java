public class Ex2 {

    public static void main(String[] args) {

        RThread RThread1 = new RThread();
        RThread RThread2 = new RThread();

        RThread1.setName("RThread1");
        RThread2.setName("RThread2");

        RThread1.start();
        RThread2.start();
    }
}

class RThread extends Thread{
    @Override
    public void run() {
        for(int i = 1; i <=12 ; i++){

            System.out.println(this.getName() + " - " + i);
            try{
                Thread.sleep(3000);
            }
            catch (Exception e){
                System.out.println("Error, sorry!");;
            }
        }
    }
} 